%% Direct solution to the discrete-time LQ-optimal control problem on a finite horizon

%% Model of the system
% Double integrator modelled (in discrete time) by $\mathbf x_{k+1}=\mathbf
% A\mathbf x_k+\mathbf B u_k$, where

A = [1 1; 0 1]; 
B = [0; 1];

n = size(A,1);  % Order of the system
m = size(B,2);  % Number of (control) inputs

%% Parameters for the optimal control problem
% We consider the popular LQ-optimal control (actually regulation here):

x0 = [1; 3];    % Initial state

N = 100;         % Time horizon (=length of the time interval)

s = [1 2];      % Diagonal entries of the weighting matrices S,   
q = [1 2];      %                                            Q, 
r = [1];        %                                        and R

S = diag(s);
Q = diag(q);
R = diag(r);

%% Building the matrices for the QP problem - the simultaneous (sparse) formulation
% See the lectures for the meaning of individual matrices here

Qbar = zeros(N*n,N*n);

for i=1:(N-1)
    Qbar((i-1)*n+1:i*n,(i-1)*n+1:i*n) = Q;
end

Qbar(end-n+1:end,end-n+1:end) = S;

Rbar = zeros(N*m,N*m);

for i=1:N
    Rbar((i-1)*m+1:i*m,(i-1)*m+1:i*m) = R;
end

Qtilde = blkdiag(Qbar,Rbar);

Bbar = zeros(N*n,N*m);

for i=1:N
    Bbar((i-1)*n+1:i*n,(i-1)*m+1:i*m) = B;
end

Abar = -eye(n*N,n*N);   % here Abar is actually Abar-I from the lecture notes 

for i=1:(N-1)
    Abar(i*n+1:(i+1)*n,(i-1)*n+1:i*n) = A;
end

Atilde = [Abar Bbar]; 

A0bar = zeros(n*N,n); 
A0bar(1:n,1:n) = A;

btilde = A0bar*x0;


%% Solving the quadratic program (QP)
% Using the quadprog from the (default) Optimization Toolbox for Matlab
% 
% First, no inequality constraints on the control and we do not exploit the
% sparsity at all.

xtilde = quadprog(Qtilde,[],[],[],Atilde,-btilde);

xopt = xtilde(1:n*N);
xopt = reshape(xopt,2,[])';
 
uopt = xtilde(n*N+1:end);

%% Plotting the solutions

t = 0:N;

figure(1)
plot(t(1:end-1),uopt,'.-')
xlabel('k')
ylabel('u')
title('Optimal control for simultaneous (sparse) format with unconstrained control')

figure(2)
plot(t,[x0';xopt],'.-')
xlabel('k')
ylabel('x') 
title('Optimal states for simultaneous (sparse) format with unconstrained control')

%% Exploiting the sparsity 
% First, lets have a look that the matrices are rather sparse indeed.
figure(3)
spy(Qtilde)

figure(4)
spy(Atilde)

% Converting the nonsparse matrices into a sparse format
Qtilde_sparse = sparse(Qtilde);
Atilde_sparse = sparse(Atilde);
btilde_sparse= sparse(btilde);

% We will just call the solver but not plot the results.
xtilde_sparse = quadprog(Qtilde_sparse,[],[],[],Atilde_sparse,-btilde_sparse);

% Apparently the solution is identical (down to rounding errors)
norm(xtilde-xtilde_sparse)

%% Comparison of solving sparse vs nonsparse solvers
tic, xtilde = quadprog(Qtilde,[],[],[],Atilde,-btilde); toc
tic, xtilde_sparse = quadprog(Qtilde_sparse,[],[],[],Atilde_sparse,-btilde_sparse); toc

%% Imposing bounds on the controls u
% We set the upper bound and the lower bound is assumed symmetric. Note
% that the the optimization vector xtilde is composed of both x, on which 
% no bounds are to be imposed, and u, on which the bounds are to be imposed.

uub = 1;
xtildeub = [repmat(Inf,n*N,1);repmat(uub,N,1)];

xtilde = quadprog(Qtilde,[],[],[],Atilde,-btilde,-xtildeub,xtildeub);

xoptc = xtilde(1:n*N);
xoptc = reshape(xopt,2,[])';
 
uoptc = xtilde(n*N+1:end);

%% Plotting the solutions
% for the upper- and lower-bounded controls.

t = 0:N;

figure(5)
plot(t(1:end-1),uoptc,'.-')
xlabel('k')
ylabel('u')
title('Optimal control for simultaneous (sparse) format - bounds on controls')

figure(6)
plot(t,[x0';xoptc],'.-')
xlabel('k')
ylabel('x') 
title('Optimal states for simultaneous (sparse) format - bounds on controls')
 
%% Building the matrices for the QP problem - the sequential (dense) formulation

Chat = zeros(N*n,N*m);
Ahat = zeros(N*n,n);
 
for i=1:N
    for j = 1:i
        Chat((i-1)*n+1:i*n,(j-1)*m+1:j*m) = A^(i-j)*B;
        Ahat((i-1)*n+1:i*n,1:n) = A^i;
    end
end

H = Chat'*Qbar*Chat + Rbar; 
F = Chat'*Qbar*Ahat;

%% Solving the set of linear equations
% Either a general solver (through the backslash operator) or a tailored
% solver based on LDL decomposition.

tic, uopts1 = -H\(F*x0); toc

%%
% But we can exploit the fact that H is symmetric

norm(H-H')

%%
%

opts.SYM = true;
tic, uopts = -linsolve(H,F*x0,opts); toc

%%
% Not much more improvements in the efficiency could be obtained by exploiting
% the sparsity in the matrices since these are generally not sparse 
% (unlike in the simultaneous framework).

%% Plotting the solution

figure(7)
plot(t(1:end-1),uopt,'.-')
xlabel('k')
ylabel('u')
title('Optimal control for sequential (dense) format')

xopts = Chat*uopts + Ahat*x0;
xopts = reshape(xopts,2,[])';

figure(8)
plot(t,[x0';xopt],'.-')
xlabel('k')
ylabel('x')
title('Optimal states for sequential (dense) format')

%% Imposing bounds on the controls u
% Using quadprog
%
% Set the upper bound. The lower bound is then understood symmetric.

uub = 1;
uub = repmat(uub,N,1);

[uoptsc,Joptsc,exitflag,output] = quadprog(H,F*x0,[],[],[],[],-uub,uub)

%% Plotting the solution

figure(9)
plot(t(1:end-1),uoptsc,'.-')
xlabel('k')
ylabel('u')
title('Optimal control for sequential (nonsparse) format - bounds on controls')

xoptsc = Chat*uoptsc + Ahat*x0;
xoptsc = reshape(xoptsc,2,[])';

figure(10)
plot(t,[x0';xoptsc],'.-')
xlabel('k')
ylabel('x')
title('Optimal states for sequential (nonsparse) format - bounds on controls')
