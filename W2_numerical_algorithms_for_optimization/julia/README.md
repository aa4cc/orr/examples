# Algorithms for unconstrained optimization

## Algorithmic (also automatic) differentiation (AD)

- forward AD, implementation using dual numbers
- reverse AD

## Unconstrained optimization

### Descent methods

- gradient (also steepest descent) method
- scaled gradient method 
- Newton's method
- Quasi-Newton methods

### Trust region methods

- trust region gradient method

## Constrained optimization

- projected gradient method 
